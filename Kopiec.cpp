#include <iostream>

using namespace std;

const int n = 12; //ilo�� element�w w zbiorze
int S = 0; //Pocz�tkowo kolejk� stanowi pusty zbi�r S

//Funkcja zamie� powoduje skopiowanie obiektu do zmiennej tymczasowej i dwa razy wywo�uje operator przypisania

void zamie�(int* a, int* b) {
    int tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

//Funkcja do uzyskania prawego syna z w�z�a drzewa
int prawysyn(int i){ return (2 * i) + 1; }

//Funkcja do uzyskania lewego syna z w�z�a drzewa
int lewysyn(int i) { return 2 * i; }

//Funkcja do uzyskania rodzica z w�z�a drzewa
int ojciec(int i) { return i / 2; }

//Procedura przywracania w�asno�ci kopca

void kopcowanie(int x[], int i) {
    int lewysyn_i = lewysyn(i);
    int prawysyn_i = prawysyn(i);

    // Znajdowanie najmniejszego spo�r�d prawego i lewego syna
    int najmniejszy = i;

    if ((lewysyn_i <= S) && (lewysyn_i > 0)) {
        if (x[lewysyn_i] < x[najmniejszy]) {
            najmniejszy = lewysyn_i;
        }
    }

    if ((prawysyn_i <= S && (prawysyn_i > 0))) {
        if (x[prawysyn_i] < x[najmniejszy]) {
            najmniejszy = prawysyn_i;
        }
    }

    if (najmniejszy != i) {
        zamie�(&x[i], &x[najmniejszy]);
        kopcowanie(x, najmniejszy);
    }
}
//Procedura buduj�ca kopiec element�w zbioru x[]
void Tw�rz_kopiec(int x[]) {
    int i;
    for (i = S / 2; i >= 1; i--) {
        kopcowanie(x, i);
    }
}

// Procedura usunMin() usuwa najmniejszy element zbioru
int usunMin(int x[]) {
    int minm = x[1];
    x[1] = x[S];
    S--;
    kopcowanie(x, 1);
    return minm;
}

void doGory(int x[], int i, int t) {
    x[i] = t;
    while ((i > 1) && (x[ojciec(i)] > x[i])) {
        zamie�(&x[i], &x[ojciec(i)]);
        i = ojciec(i);
    }
}

void wDol(int x[], int i, int t) {
    x[i] = t;
    kopcowanie(x, i);
}

//Procedura wstaw(t) wstawia do kolejki nowy element t
void wstaw(int x[], int t) {
    S++;
    doGory(x, S, t);
}


int main() {
    int x[n]; 

    wstaw(x, 12);
    wstaw(x, 20);
    wstaw(x, 15);
    wstaw(x, 29);
    wstaw(x, 23);
    wstaw(x, 17);
    wstaw(x, 22);
    wstaw(x, 35);
    wstaw(x, 40);
    wstaw(x, 26);
    wstaw(x, 51);
    wstaw(x, 19);

    cout << "Nieposortowane dane: ";
    for (int i = 1; i <= n; i++) {   
        cout << x[i] << " ";
    }

   
    cout << "\nPosortowane dane:    ";
  
    for (int i = 1; i <= n; i++) {
       cout << usunMin(x) << " ";
    }


    system("PAUSE");
    return 0;
}
